import { Component, Input, OnInit } from '@angular/core';
import { Intro } from '../models/home';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
@Input() intro!: Intro;
public boton: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  onButtonClick() : void {
    this.boton = !this.boton;
   }
}
