import { Article } from './../models/home';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-last',
  templateUrl: './last.component.html',
  styleUrls: ['./last.component.scss']
})
export class LastComponent implements OnInit {
@Input() last!: Article;
public flag: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }
  onButtonClick() : void {
    this.flag = !this.flag;
  }
}
