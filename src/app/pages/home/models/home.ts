export interface Home {
    intro: Intro;
    middle: Array<Article>;
    last: Array<Article>;
}

export interface Intro {
    img: Image;
    descripcion: string;
    moreinfo?: string;
}

export interface Image {
    img: string;
    name: string;
}

export interface Article {
    img: Image;
    title: string;
    subtitle: string;
}
