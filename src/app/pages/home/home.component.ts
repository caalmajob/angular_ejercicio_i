import { Home } from './models/home';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public home: Home;
  constructor() {
    this.home = {
      intro: {
        img: {
          img: 'https://www.indiegamewebsite.com/wp-content/uploads/2018/05/GreatestHeader2.jpg',
          name: 'Indie Games',
        },
        descripcion: 'Indie Game nació un día de febrero de 2021 sin un objetivo claro. Lo único que nos planteamos en su momento fue ser honestos con nosotros mismos, esperando que los diez o doce lectores a los que aspirábamos pudieran, al menos, disfrutar de unos textos alejados de la imparcialidad a la que tendía la crítica en aquel momento. Desde aquel momento muchas cosas han cambiado, principalmente en nuestras cabezas, las cuales son un yermo sobre el que los ancianos del lugar recuerdan que una vez estuvo poblado.',
        moreinfo: 'Aquí hablaremos de LO INDIE y la industria patria. Sí, lo sabemos. Nadie sabe exactamente qué es LO INDIE, así que puedes cambiarlo por "el circuito menos comercial" o cualquier otra definición con la que te sientas cómodo. Nuestro objetivo en Nivel Oculto es reivindicar esa otra forma de hacer videojuegos, proporcionando una plataforma en la que los interesados en el desarrollo independiente puedan encontrar artículos y noticias de su preferencias, mientras intentamos descubrir títulos interesantes para aquellos menos interesados en LO INDIE.',
      },
      middle: [
        { img: {
          img: 'https://i2.wp.com/www.banwoh.com/wp-content/uploads/2020/05/The-Almost-Gone-android-ios.jpg?w=800&ssl=1',
          name: 'The Almost Gone',
        },
        title: 'The Almost Gone',
        subtitle: 'y por qué los videojuegos (y las cosas) son un humanismo'},
        { img: {
          img: 'https://niveloculto.com/wp-content/uploads/2020/11/afterparty.jpg',
          name: 'AfterParty',
        },
        title: 'AfterParty',
        subtitle: 'El alcoholismo y la perdida'},
        { img: {
          img: 'https://niveloculto.com/wp-content/uploads/2021/02/haven.jpg',
          name: 'Heaven',
        },
        title: 'Analisis: Heaven',
        subtitle: 'Somos dos y un reloj'},
        { img: {
          img: 'https://niveloculto.com/wp-content/uploads/2021/02/vanishing-770x433.png',
          name: 'Vainishing Grace',
        },
        title: 'Vainishing Grace',
        subtitle: 'En el nuevo mundo'},
        { img: {
          img: 'https://cdn.cloudflare.steamstatic.com/steam/apps/367520/capsule_616x353.jpg?t=1601950406',
          name: 'Hollow Knight',
        },
        title: 'Hollow Knight',
        subtitle: 'La busqueda de la identidad'},
        { img: {
          img: 'https://f.rpp-noticias.io/2018/12/06/354135_722591.png',
          name: 'Celeste',
        },
        title: 'Celeste',
        subtitle: 'Un viaje de superación'}
      ],
      last:[{ img: {
        img: 'https://sm.ign.com/t/ign_it/screenshot/default/the-game-awards-2017_296f.1280.jpg',
        name: 'The Game Awards',
      },
      title: 'The Game Awards',
      subtitle: 'Juegos del 2020 nominados'},
      { img: {
        img: 'https://static.thinkmobiles.com/uploads/2019/02/best-indie-games.webp',
        name: 'Top 10 Indie Games',
      },
      title: 'Top 10 Indie Games',
      subtitle: 'Los 10 mejores juegos del 2020 elegidos por nuestros lectores'},
      { img: {
        img: 'https://i.ytimg.com/vi/FJaUCiir-20/maxresdefault.jpg',
        name: 'Best Games E3',
      },
      title: 'Best Indie Games',
      subtitle: 'Novedades presentadas'},
      { img: {
        img: 'https://i.blogs.es/161c0f/igf/1366_2000.jpg',
        name: 'Indie Game Festival',
      },
      title: 'Indie Game Festival',
      subtitle: 'Más de 100 nuevos juegos presentados'},
    ],
    };
  }

  ngOnInit(): void {
  }

}
