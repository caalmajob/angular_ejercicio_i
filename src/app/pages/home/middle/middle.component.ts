import { Article } from './../models/home';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-middle',
  templateUrl: './middle.component.html',
  styleUrls: ['./middle.component.scss']
})
export class MiddleComponent implements OnInit {
  @Input() middle: Article;
  constructor() { }

  ngOnInit(): void {
  }

}
