import { Header } from './models/header';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public header: Header;
  public flag: boolean = false;
  constructor() { 
    this.header = {
      icon: {
        img: '/assets/images/logo.png',
        name: 'indie game logo',
      },
      img: 'https://images-na.ssl-images-amazon.com/images/I/61LNAo2K9RL.png',
      links: ['HOME', 'REVIEWS', 'RELEASES', 'AWARDS'],
    }
  }

  ngOnInit(): void {
  }
onButtonClick() : void {
    this.flag = !this.flag;
   }

}
