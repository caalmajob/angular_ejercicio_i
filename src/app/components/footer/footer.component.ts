import { Component, OnInit } from '@angular/core';
import { Footer } from './models/footer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footer: Footer;
  constructor() {
    this.footer = {
      icon: [{
        img: '/assets/images/instagram.svg',
        name: 'instagram logo',
      },
      {
        img: '/assets/images/facebook.svg',
        name: 'Facebook logo',
      },
      {
        img: '/assets/images/twitter.svg',
        name: 'Twitter logo',
      },
      {
        img: '/assets/images/youtube.svg',
        name: 'Youtube logo',
      }],
    }
  }

  ngOnInit(): void {
  }

}

