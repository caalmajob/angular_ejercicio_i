export interface Footer {
    icon: Array<Rrss>;
    info?: Array<string>;
}

export interface Rrss {
    img: string;
    name: string;
}